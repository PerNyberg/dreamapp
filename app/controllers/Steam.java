package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import play.libs.Json;
import play.api.libs.json.*;
import play.libs.ws.*;
import play.libs.F.Function;
import play.libs.F.Promise;
import com.fasterxml.jackson.databind.JsonNode;

import models.*;

/**
 * Class to access the steam API. Will be accessed from the client side of the web application through our own API.
 */

public class Steam extends Controller {	
	
	private static final String STEAM_API_KEY = "6517D11707D21BB7EA3BF12D875E3B89";
	
	/**
	 * Takes a steam user id and retrieves this users steam information from the steam API
	 * Then returns a promise of a JsonNode containing this users steam info
	 *
	 * @param userID	A steam user id
	 * @return      	A Promise<JsonNode>
	 */
	public static Promise<JsonNode> getUserInfo(String userID) {
		Promise<JsonNode> jsonPromise = WS.url("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key="+STEAM_API_KEY+"&steamids="+userID).get().map(
			new Function<WSResponse, JsonNode>() {
				public JsonNode apply(WSResponse response) {
					JsonNode json = response.asJson();
					return json;
				}
			}
		);
		return jsonPromise;
	}
}