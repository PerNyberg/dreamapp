package controllers;

import java.util.List;
import java.util.ArrayList;

import views.html.*;
import views.*;
import java.sql.*;
import models.*;

import play.*;
import play.mvc.*;
import play.libs.Json;
import play.api.libs.json.*;
import play.mvc.BodyParser; 
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import play.mvc.Http.Request;
import play.mvc.Http.Context;
import play.api.mvc.Cookie;
import javax.xml.ws.http.HTTPException;

public class Application extends Controller {

	private static Database db = new Database();

	public static Result preflight(String all) {
		response().setHeader("Access-Control-Allow-Origin", "*");
		response().setHeader("Allow", "*");
		response().setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Referer, User-Agent");
		return ok();
	}
	
    public static Result addUser(String userID, String userName) throws SQLException {
		System.out.println("AddUser was called with name: "+userName);
		if(db.addUser(userID,userName)){
			System.out.println("New user with userID "+userID+" added. /addUser");
			return created("User added. /addUser");
		}else{
			System.out.println("User already existed. /addUser");
			return ok("User already exists. /addUser");
		}
	}
	
	public static Result setName(String userID,String name) throws SQLException {
		if(db.setName(userID,name)){
			return created("Users name set. /setName/application");
		}else{
			return ok("User did not exist. /setName/application");
		}
	}
	
	public static Result userExists(String userID) throws SQLException {
		if(db.userExists(userID)){
			return found("User exists");
		}else{
			return notFound("User does not exist");
		}
	}
	
	public static Result removeUser(String userID) throws SQLException {
		db.removeUser(userID);
		return ok("This userID has been removed if it existed");
	}
	
	public static Result leaveTeam(String userID,String teamName) throws SQLException {
		db.removeUserFromTeam(teamName,userID);
		return redirect("/team/"+userID);
	}
	
	public static Result login() {
		return ok(login.render("Everything is fine"));
	}
	
	public static Result logout(){
		response().discardCookie("DreamApp");
		return redirect("/login");
	}

	public static Result addAchievementToUser(String userID, String code) throws SQLException{
		if(db.addAchievementToUser(userID, code)){
			return ok(achievements.render(db.getAllAchievements(userID)));
		}
		return ok(achievements.render(db.getAllAchievements(userID)));
	}
	
	public static Result createTeam(String userID, String teamName) throws SQLException{
		System.out.println("Create team was called with userID: "+userID+" and teamName: "+teamName);
		if(db.userHasTeam(userID)== null){	// Checking to make sure user is not already in a team
			db.createTeam(userID,teamName);
			System.out.println("Team was created");
			return ok(myTeam.render(db.getTeam(teamName)));
		}else{
			return internalServerError("not allowed, user already in team");
		}
	}
	
	public static Result joinTeam(String userID, String teamName) throws SQLException{
		if(db.addUserToTeam(teamName,userID)){
			System.out.println("User was added to team "+teamName+" successfully");
			return redirect("/team/"+userID);
		}else{
			System.out.println("User was not addded to team "+teamName+" successfully");
			return ok("User not added successfully");
		}
	}
	
	public static Result memberOfTeam(String userID) throws SQLException{
		return ok("ok");
	}
	
	public static Result toggleAttendance(String userID, String eventName) throws SQLException{
		db.toggleAttendance(userID,eventName);
		return ok("Toggled userID's: "+userID+" attendance for "+eventName);
	}
	
	public static Result schedule(String userID) throws SQLException{
		Schedule fullSchedule = db.getFullSchedule(userID);
		Schedule mainEvents = new Schedule();
		Schedule preEvents = new Schedule();
		for(Event e : fullSchedule.getAllEvents()){
			if(e.getDHDay().equals("0")){
				System.out.println(e.getName()+" was preEvent");
				preEvents.addEvent(e);
			}else{
				System.out.println(e.getName()+" was not preEvent and its DHDAY was: "+e.getDHDay()+" and the DHDays type was: "+e.getDHDay().getClass());
				mainEvents.addEvent(e);
			}
		}
		return ok(schedule.render(mainEvents,preEvents));
	}

    public static Result home(String userID) throws SQLException {
		Schedule mySchedule = db.getMySchedule(userID);
		List<Achievement> myAchievements = db.getUsersAchievements(userID);
		int myPoints = db.getUsersPoints(userID);
		return ok(home.render(mySchedule, myAchievements, myPoints));
    }
	
	public static Result home2(){		// used when an already logged in user enters the homepage
		return ok(home.render(null, null, null));
    }

    public static Result stream() {
    	return ok(stream.render());
    }
    
    public static Result achievements(String userID) throws SQLException{
        return ok(achievements.render(db.getAllAchievements(userID)));
    }

    public static Result achievementsTeam(String userID) throws SQLException {
		String usersTeam = db.userHasTeam(userID);
		if(usersTeam == null){
			return ok(achievementsTeam.render(null));
		}else{
			return ok(achievementsTeam.render(db.getTeamAchievements(usersTeam)));
		}
    }

    public static Result team(String userID) throws SQLException {
		String usersTeam = db.userHasTeam(userID);
		if(usersTeam != null){
			// render the teams page with arguments to the template that contain the teams name and members
			return ok(myTeam.render(db.getTeam(usersTeam)));
		}else{
			// else render the create team or join team page
			return ok(team.render());
		}
    }
	
	public static Result getAllUsers() throws SQLException{
		ArrayList<User> allUsers = db.getAllUsers();
		return ok(leaderboards1.render(allUsers));
	}
	
	public static Result getAllTeams() throws SQLException{
		System.out.println("-------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------");
		ArrayList<Team> allTeams = db.getAllTeams();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------");
		return ok(leaderboards2.render(allTeams));
	}
    
    public static Result map() {
        return ok(map.render());
    }

    public static Result messenger() {
        return ok(messenger.render());
    }
	
	public static Result teamList() throws SQLException{
		return ok(teamList.render(db.getAllTeamsMin()));
	}
}