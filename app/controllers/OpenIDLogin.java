package controllers;

import java.util.HashMap;
import java.util.Map;
import java.sql.*;

import play.data.DynamicForm;
import play.data.Form;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.openid.OpenID;
import play.libs.openid.OpenID.UserInfo;
import play.mvc.Controller;
import play.mvc.Result;
import play.Logger;

import play.libs.Json;
import play.api.libs.json.*;
import play.mvc.BodyParser; 
import play.libs.ws.*;
import play.libs.F.Function;
import play.libs.F.Promise;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import models.*;

public class OpenIDLogin extends Controller {
	 
	private static Database db = new Database();
	
	public static Result loginPost() {	
		String redirectUrl = "https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.return_to=https%3A%2F%2Fapple-pie-2645.herokuapp.com%2FopenID%2Fcallback&openid.realm=https%3A%2F%2Fapple-pie-2645.herokuapp.com";
		return redirect(redirectUrl);
	}
	
	public static Result openIDCallback() throws SQLException {
		Promise<UserInfo> userInfoPromise = OpenID.verifiedId();
		UserInfo userInfo = userInfoPromise.get(5000);
		JsonNode json = Json.toJson(userInfo);
		String steamId = json.toString().replaceAll("[^0-9]","");
		JsonNode steamUserJson = Steam.getUserInfo(steamId).get(5000);			// .get() not ideal, fix
		String steamUserName = steamUserJson.findPath("personaname").textValue();
		db.addUser(steamId,steamUserName);
		response().setCookie("userID",steamId);
		return redirect("/home/"+steamId);		
	}
}
