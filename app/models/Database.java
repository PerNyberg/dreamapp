package models;

import java.util.Collections;
import java.util.ArrayList;
import java.sql.*;
import play.db.*;
import javax.sql.*;
import models.*;

/**
 * Database class to handle the connection between a MySQL database 
 * and the client side of a web application. The database class is instantiated
 * in the Application.java controller class.
 */

public class Database{
	
	Connection connection;

	public Database(){
		
	}
	/**
	 * Closes the connection to the database.
	 *
	 * @return      	void
	 */
	public void close() throws SQLException{
		try{
			connection.close();
		}catch(SQLException e){
			System.out.println("Close error "+e);
		}
	}
	/**
	 * Takes a query String, connects to the database, creates a statement and executes the update.
	 *
	 * @param query		String with JDBC-compliant insert/update statement
	 * @return      	void
	 */
	public void prepareUpdate(String query) throws SQLException{
		connection = DB.getConnection();
		Statement stmt = connection.createStatement();
		stmt.executeUpdate(query);
	}
	/**
	 * Takes a query String, connects to the database, creates a statement and executes the select statement.
	 *
	 * @param query		String with JDBC-compliant select statement
	 * @return      	ResultSet containing the selected data from the database
	 */
	public ResultSet prepareSelect(String query) throws SQLException{	// help method used for selects
		connection = DB.getConnection();
		Statement stmt = connection.createStatement();
		return stmt.executeQuery(query);		// returns the result of the select query
	}
	/**
	 * Inserts a new user into the database. 
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @param userName	The users real name taken from the Facebook api
	 * @return      	Returns boolean of true if user was added, otherwise false
	 */
	public boolean addUser(String userID, String userName) throws SQLException{
		long userIDInt = Long.parseLong(userID); 
		if(!userExists(userID)){
			try{
				prepareUpdate("INSERT INTO Person " + "VALUES ('"+userID+"','"+userName+"')");
				close();
				return true;
			}catch (SQLException e){
				System.out.println("SQL query error: "+e);
				close();
				return false;
			}
		}
		close();
		return false;
	}
	/**
	 * Sets the name of a userID in the database. 
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @return      	Returns boolean of true if user existed and no SQL error happened, otherwise false
	 */
	public boolean setName(String userID, String name) throws SQLException{
		long userIDInt = Long.parseLong(userID); 
		if(userExists(userID)){
			try{
				prepareUpdate("UPDATE Person SET Name = '"+name+"' WHERE Person.PersonID = '"+userID+"'");
				close();
				return true;
			}catch (SQLException e){
				System.out.println("SQL query error: "+e);
				close();
				return false;
			}
		}
		close();
		return false;
	}
	/**
	 * Checks if a particular userID exists in the database.
	 *
	 * @param userID	String that contains the userID to look for in the database
	 * @return      	Returns boolean of true if user existed, otherwise false
	 */
	public boolean userExists(String userID) throws SQLException{
		long userIDInt = Long.parseLong(userID);
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Person WHERE PersonID='"+userID+"'");
			if(!rs.next()){
			   close();
			   return false;
			}
			close();
			return true;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return false;
		}
	}
	/**
	 * Removes a user from the database. 
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @return      	Returns boolean of true if user was added, otherwise false
	 */
	public void removeUser(String userID) throws SQLException{
		long userIDInt = Long.parseLong(userID);
		try{
			prepareUpdate("DELETE * FROM Person WHERE PersonID='"+userIDInt+"'");
		}catch(SQLException e){
			System.out.println(e);
		}
		close();
	}
	/**
	 * Returns a Schedule-class with the events that a user is planning to attend.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @return      	Returns null if username was undefined, if SQL-queries fail or if user is not attending any events, otherwise returns an instance of the Schedule class
	 */
	public Schedule getMySchedule(String userID) throws SQLException{
		if(userID.equals("undefined")){
			close();
			return null;
		}
		long userIDInt = Long.parseLong(userID);
		Schedule mySchedule = new Schedule();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Event, Attendance WHERE Attendance.PersonID="+userID+" AND Attendance.EventName = Event.EventName");
			while(rs.next()){
				mySchedule.addEvent(new Event(rs.getString("EventName"),rs.getString("Description"),rs.getString("ImagePath"),rs.getString("DHDay"),true,Integer.parseInt(rs.getString("Month")),Integer.parseInt(rs.getString("Day")),Integer.parseInt(rs.getString("Hour")),Integer.parseInt(rs.getString("Minute"))));
			}
			close();
			if(mySchedule.isEmpty()){
				return null;
			}else{
				return mySchedule;
			}
		}catch(SQLException e){
			System.out.println(e);
		}
		close();
		return null;
	}
	/**
	 * Returns a Schedule instance which contains all events in the database.
	 * Takes a users id so the events Attending field can be set to true or false for different users.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @return      	Returns a Schedule instance which contains all the events, null if error
	 */
	public Schedule getFullSchedule(String userID) throws SQLException{
		Schedule fullSchedule = new Schedule();
		Schedule mySchedule = getMySchedule(userID);
		ArrayList<String> attendEvents = new ArrayList<String>();		// Will contain all the names of the events which the user is attending
		if(mySchedule != null){
			for(Event e : mySchedule.getAllEvents()){
				attendEvents.add(e.getName());
			}
		}
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Event");
			while(rs.next()){
				boolean attendance = false;
				if(attendEvents.contains(rs.getString("EventName"))){
					attendance = true;
				}
				fullSchedule.addEvent(new Event(rs.getString("EventName"),rs.getString("Description"),rs.getString("ImagePath"),rs.getString("DHDay"),attendance,Integer.parseInt(rs.getString("Month")),Integer.parseInt(rs.getString("Day")),Integer.parseInt(rs.getString("Hour")),Integer.parseInt(rs.getString("Minute"))));
			}
			close();
			return fullSchedule;
		}catch(SQLException e){
			System.out.println(e);
		}
		close();
		return null;
	}
	/**
	 * Creates a new team which contains a person. Returns true if team was created, false if not.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The users userID taken from the Facebook api
	 * @param teamName	The name of the new team
	 * @return      	Returns boolean of true if team was created, false if not
	 */
	public boolean createTeam(String userID, String teamName) throws SQLException{	
		try{
			prepareUpdate("INSERT INTO Team " + "VALUES ('"+teamName+"',0)");
			prepareUpdate("INSERT INTO Member " + "VALUES ('"+userID+"','"+teamName+"')");
			close();
			return true;
		}catch (SQLException e){
			System.out.println(e);
			close();
			return false;
		}
	}
	/**
	 * Checks if a team exists, returns true if team exists or false otherwise.
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team to be checked for
	 * @return      	Returns boolean of true if team existed, otherwise false
	 */
	public boolean teamExists(String teamName) throws SQLException{	
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Team WHERE Team.TeamName='"+teamName+"'");
			if(rs.next() == false){	// no team existed
				close();
				return false;
			}else{					// team existed
				close();
				return true;
			}
		}catch (SQLException e){
			System.out.println(e);
			close();
			return false;
		}
	}
	/**
	 * Checks if a particular userID exists in a team
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team to check for the userID in
	 * @param userID	The name of the user to check for in the specified team
	 * @return      	Returns boolean of true if user existed in team, otherwise false
	 */
	public boolean userExistsInTeam(String teamName, String userID) throws SQLException{	
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Member WHERE Member.TeamName='"+teamName+"' AND Member.PersonID='"+userID+"'");
			if(rs.next() == false){	// user did not exist in team
				close();
				return false;
			}else{					// user existed in team
				close();
				return true;
			}
		}catch (SQLException e){
			System.out.println(e);
			close();
			return false;
		}
	}
	/**
	 * Adds a user to a particular team
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team to add the user to
	 * @param userID	The name of the user to add to the team
	 * @return      	Returns boolean of true if user was added, otherwise false
	 */
	public boolean addUserToTeam(String teamName, String userID) throws SQLException{	
		if(userHasTeam(userID) == null){
			try{
				prepareUpdate("INSERT INTO Member " + "VALUES ('"+userID+"','"+teamName+"')");
				close();
				return true;
			}catch (SQLException e){
				System.out.println(e);
				close();
				return false;
			}
		}else{
			close();
			return false;
		}
	}
	/**
	 * Removes a user from a team. If team is now empty, it is deleted
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team to remove the user from
	 * @param userID	The userID of the user to remove from the team
	 * @return      	Returns a true if query did not produce error, otherwise false
	 */
	public boolean removeUserFromTeam(String teamName, String userID) throws SQLException{	
		try{
			prepareUpdate("DELETE FROM Member WHERE Member.PersonID='"+userID+"' AND Member.TeamName='"+teamName+"'");
			ResultSet rs = prepareSelect("SELECT * FROM Member WHERE Member.TeamName = '"+teamName+"'");
			if(rs.next() == false){
				System.out.println("Team has no members, delete it");
				prepareUpdate("DELETE FROM Team WHERE Team.TeamName='"+teamName+"'");
			}
			close();
			return true;
		}catch (SQLException e){
			System.out.println(e);
			close();
			return false;
		}
	}
	/**
	 * Returns the real name of a user from the persons userID.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The userID of the user whose name will be returned
	 * @return      	Returns a String containing the users real name, null if SQLException
	 */
	public String getUserNameFromId(String userID) throws SQLException{
		String userName;
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Person WHERE Person.PersonID='"+userID+"'");
			rs.next();
			userName = rs.getString("Name");
			close();
			return userName;
		}catch(SQLException e){
			System.out.println("getUserNameFromID: SQL Exception: "+e);
			close();
			return null;
		}
	}
	/**
	 * Returns an instance of the Team class from the name of a team
	 * Fetches the information about a particular team from the database from the team name, then returns a Team instance.
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team which will be returned
	 * @return      	Returns an instance of the Team class, null if SQLException
	 */
	public Team getTeam(String teamName) throws SQLException{
		Team team = new Team(teamName);
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Member WHERE Member.TeamName='"+teamName+"'");
			while(rs.next()){
				team.addMember(new User(Long.parseLong(rs.getString("PersonID")),getUserNameFromId(rs.getString("PersonID")),5));
			}
			close();
			return team;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}
	/**
	 * Returns an ArrayList containing a all the users in the database, represented by the User class.
	 * Executes the query, and then closes the connection.
	 *
	 * @return      	Returns an ArrayList of Team instances, null if SQLException
	 */
	public ArrayList<User> getAllUsers() throws SQLException{			// make it so that every players points are compiled when new user classes are created
		ArrayList<User> allUsers = new ArrayList<User>();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Person");
			while(rs.next()){
				ResultSet user = prepareSelect("SELECT * FROM HasGotAchievement,Achievement WHERE HasGotAchievement.PersonID = '"+rs.getString("PersonID")+"' AND HasGotAchievement.AchievementID = Achievement.AchievementID");
				int points = 0;
				while(user.next()){
					System.out.println(user.getString("poäng"));
					points += Integer.parseInt(user.getString("poäng"));
				}
				User u = new User(Long.parseLong(rs.getString("PersonID")),rs.getString("Name"),points);
				u.addAchievements(getUsersAchievements(rs.getString("PersonID")));
				allUsers.add(u);
			}
			close();
			Collections.sort(allUsers);
			return allUsers;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}

	/**
	 * Returns a String of the name of a users team.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The userID of the user whose teams name will be returned
	 * @return      	Returns a String of a users team's name, null if not in a team
	 */
	public String userHasTeam(String userID) throws SQLException{		// returns the name of the users team, otherwise null
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Member WHERE Member.PersonID='"+userID+"'");
			if(!rs.next()){
				close();
				return null;
			}else{
				close();
				return rs.getString("TeamName");
			}
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}
	/**
	 * Toggles the attendance for a certain users attendance for an event in the database
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The userID of the user whose attendance for a certain event will be toggled
	 * @param eventName	The name of the event to toggle attendance for
	 * @return      	void
	 */
	public void toggleAttendance(String userID,String eventName) throws SQLException{		// returns the name of the users team, otherwise null
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Attendance WHERE Attendance.PersonID='"+userID+"' AND Attendance.EventName='"+eventName+"'");
			if(rs.next() == false){
				prepareUpdate("INSERT INTO Attendance " + "VALUES ('"+userID+"','"+eventName+"')");
			}else{
				prepareUpdate("DELETE FROM Attendance WHERE Attendance.PersonID='"+userID+"' AND Attendance.EventName='"+eventName+"'");
			}
		}catch(SQLException e){
			System.out.println(e);
		}
		close();
	}
	/**
	 * Returns an ArrayList of all teams, represented by the Team class.
	 * Executes the query, and then closes the connection.
	 *
	 * @return      	ArrayList containing Team instances, null if SQLException
	 */
	public ArrayList<Team> getAllTeams() throws SQLException{
		ArrayList<User> allUsers = getAllUsers();
		ArrayList<Team> allTeams = new ArrayList<Team>();
		
		ResultSet rs = prepareSelect("SELECT * FROM Team");

		while(rs.next()){
			System.out.println("Processing team: "+rs.getString("TeamName"));
			Team team = new Team(rs.getString("TeamName"));
			ResultSet member = prepareSelect("SELECT * FROM Member WHERE Member.TeamName = '"+rs.getString("TeamName")+"'");
			while(member.next()){
				System.out.println("Processing user: "+member.getString("PersonID"));
				team.addMember(new User(Long.parseLong(member.getString("PersonID")),getUserNameFromId(member.getString("PersonID")),0));
				team.addAchievements(getUsersAchievements(member.getString("PersonID")));
			}
			allTeams.add(team);
			close();
		}
		close();
		Collections.sort(allTeams);
		return allTeams;
	}
	/**
	 * Returns an ArrayList of all teams, represented by the Team class. Retrives minimal information to be faster
	 * Executes the query, and then closes the connection.
	 *
	 * @return      	ArrayList containing Team instances, null if SQLException
	 */
	public ArrayList<Team> getAllTeamsMin() throws SQLException{
		ArrayList<Team> allTeams = new ArrayList<Team>();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Team");
			while(rs.next()){
				allTeams.add(new Team(rs.getString("TeamName")));
			}
			close();
			return allTeams;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}

	/**
	* Returns the total amount of points for a specified user.
	* Executes the query, and then closes the connection.
	*
	* @param userID 	The userID of the user whose points will be retrieved
	* @return 			An int value representing the total amount of points for a user, 0 if SQLException
	*/
	public int getUsersPoints(String userID) throws SQLException{
		try{
			int totalPoints = 0;
			for (Achievement achievement : getUsersAchievements(userID)){
				totalPoints += achievement.getPoints();
			}
			close();
			return totalPoints;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return 0;
		}
	}

	/**
	 * Returns an ArrayList of all achievements, represented by the Achievement class.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The userID of the user who is requesting to see the achievement page
	 * @return      	ArrayList containing Achievement instances, null if SQLException
	 */
	public ArrayList<Achievement> getAllAchievements(String userID) throws SQLException{
		ArrayList<Achievement> allAchievements = new ArrayList<Achievement>();
		ArrayList<String> myCompletedAchievements = new ArrayList<String>();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM HasGotAchievement WHERE PersonID = '"+userID+"'");
			while(rs.next()){
				myCompletedAchievements.add(rs.getString("AchievementID"));
			}
			rs = prepareSelect("SELECT * FROM Achievement");
			while(rs.next()){
				if(myCompletedAchievements.contains(rs.getString("AchievementID"))){
					allAchievements.add(new Achievement(rs.getString("namn"),rs.getString("description"),Integer.parseInt(rs.getString("poäng")),true));
				}else{
					allAchievements.add(new Achievement(rs.getString("namn"),rs.getString("description"),Integer.parseInt(rs.getString("poäng")),false));
				}
			}
			close();
			return allAchievements;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}


	/** Adds an achievement to a user in the HasGotAchievement table  */
	public boolean addAchievementToUser(String userID, String code) throws SQLException{
	
		ArrayList<String> myCompletedAchievements = new ArrayList<String>();

		try{
			ResultSet rs = prepareSelect("SELECT * FROM HasGotAchievement WHERE PersonID = '"+userID+"'");
			while(rs.next()){
				myCompletedAchievements.add(rs.getString("AchievementID"));
			}
			rs = prepareSelect("SELECT * FROM Achievement WHERE Code = '"+code+"'");
			rs.next();
			if(!myCompletedAchievements.contains(rs.getString("AchievementID")) && rs.getString("Code").equals(code)){
				prepareUpdate("INSERT INTO HasGotAchievement " + "VALUES ('"+rs.getString("AchievementID")+"','"+userID+"')");
				close();
				return true;
			}else{
				close();
				return false;
			}
		}catch(SQLException e){
			System.out.print(e);
			close();
			return false;
		}
	}
	
	/**
	 * Returns an ArrayList of a users achievements, represented by the Achievement class.
	 * Executes the query, and then closes the connection.
	 *
	 * @param userID	The userID of the user whose achievements will be retrieved
	 * @return      	ArrayList containing Achievement instances, null if SQLException
	 */
	public ArrayList<Achievement> getUsersAchievements(String userID) throws SQLException{
		ArrayList<Achievement> myCompletedAchievements = new ArrayList<Achievement>();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM HasGotAchievement,Achievement WHERE HasGotAchievement.PersonID = '"+userID+"' AND HasGotAchievement.AchievementID = Achievement.AchievementID");
			while(rs.next()){
				myCompletedAchievements.add(new Achievement(rs.getString("namn"),rs.getString("description"),Integer.parseInt(rs.getString("poäng")),true));
			}
			close();
			return myCompletedAchievements;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}
	/**
	 * Returns an ArrayList of a teams achievements, represented by the Achievement class.
	 * Executes the query, and then closes the connection.
	 *
	 * @param teamName	The name of the team whose achievements will be retrieved
	 * @return      	ArrayList containing Achievement instances, null if SQLException
	 */
	public ArrayList<Achievement> getTeamAchievements(String teamName) throws SQLException{
		ArrayList<Achievement> teamAchievements = new ArrayList<Achievement>();
		try{
			ResultSet rs = prepareSelect("SELECT * FROM Member,HasGotAchievement WHERE Member.TeamName = '"+teamName+"' AND HasGotAchievement.PersonID = Member.PersonID");
			while(rs.next()){
				teamAchievements.add(new Achievement(rs.getString("namn"),rs.getString("description"),Integer.parseInt(rs.getString("poäng")),true));
			}
			close();
			return teamAchievements;
		}catch(SQLException e){
			System.out.println(e);
			close();
			return null;
		}
	}
}