package models;
import java.text.DecimalFormat;
import org.joda.time.DateTime;

public class Event implements Comparable<Event> {

	private String name;
	private String description;
	private DateTime date;
	private String imagePath;
	private String dhDay;
	private boolean attendance;
	DecimalFormat df;

	public Event(String name, String de, String imagePath, String dayOfDH, boolean attend, int month, int day, int hour, int minute) {
		System.out.println("Event instance initiating, month is "+month+", day is "+day+", hour is "+hour+", minute is "+minute);
		this.name = name;
		this.description = de;
		this.imagePath = imagePath;
		dhDay = dayOfDH;
		attendance = attend;
		date = new DateTime(2015,month,day,hour,minute);
		df = new DecimalFormat("00");
	}

	public String getName() {
		return name;
	}
	
	public boolean getAttendance(){
		return attendance;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDHDay(){
		return dhDay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return df.format(date.getDayOfMonth()) + "/" + df.format(date.getMonthOfYear());
	}

	public DateTime getDateElement() {
		return date;
	}
	
	public String getImagePath(){
		return imagePath;
	}

	public void setDate(int month, int day) { // May be broken and may be causing a NullPointerException
		date.withMonthOfYear(month);
		date.withDayOfMonth(day);
	}

	public String getTime() {
		return df.format(date.getHourOfDay()) + ":" + df.format(date.getMinuteOfHour());
	}

	public void setTime(int hour, int minute) {
		date.withHourOfDay(hour);
		date.withMinuteOfHour(minute);
	}

	@Override
	public int compareTo(Event o) {
		return this.getDateElement().compareTo(o.getDateElement());
	}
}
