package models;

import java.util.ArrayList;
import java.util.List;

public class User implements Comparable<User> {

	private Long steamID;
	private String userName;
	private int points;
	private List<Achievement> achievements = new ArrayList<Achievement>();
	private List<Event> userEvents = new ArrayList<Event>();

	public User(Long steamID, String userName, int points) {
		this.steamID = steamID;
		this.userName = userName;
		this.points = points;
	}

	public Long getID() {
		return steamID;
	}
	
	public String getName(){
		return userName;
	}
	
	public void setName(String name){
		userName = name;
	}
	
	public int getPoints(){
		return points;
	}

	public List<Achievement> getAchievements() {
		//return new ArrayList<Achievement>(achievements);
		return achievements;
	}

	public List<Event> getUserEvents() {
		return new ArrayList<Event>(userEvents);
	}

	public boolean addAchievement(Achievement ach) {
		return achievements.add(ach);
	}
	
	public void addAchievements(ArrayList<Achievement> myAchievements){
		achievements = myAchievements;
	}

	public boolean addEvent(Event ev) {
		return userEvents.add(ev);
	}
	
	public int compareTo(User other) {
		return Integer.compare(other.points, this.points);
	}
}
