package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.joda.time.DateTime;

public class Schedule {

	private Set<Event> events = new TreeSet<Event>();

	public ArrayList<Event> getAllEvents() {
		return new ArrayList<Event>(events);
	}

	public boolean addEvent(Event ev) {
		return events.add(ev);
	}

	public boolean removeEvent(Event ev) {
		return events.remove(ev);
	}

	public boolean isEmpty() {
		return events.isEmpty();
	}

	public Event getNextEvent() {

		DateTime currentTime = new DateTime();

		for (Event e : events) {
			if (currentTime.isAfter(e.getDateElement())) {
				return e;
			}
		}
		return null;
	}
}
