package models;

public class Achievement {

	private String name;
	private String description;
	private int points;
	private boolean completed;

	public Achievement(String name, String description, int points, boolean completed) {
		this.name = name;
		this.description = description;
		this.points = points;
		this.completed = completed;
	}

	public String getName() {
		return name;
	}
	
	public boolean getCompleted() {
		return completed;
	}

	public void setName() {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

}
