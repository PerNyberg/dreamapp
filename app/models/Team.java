package models;

import java.util.ArrayList;
import java.util.List;

public class Team implements Comparable<Team>{

	private List<User> teamMembers = new ArrayList<User>();
	private List<Achievement> teamAchievements = new ArrayList<Achievement>();
	private String teamName;
	private int points;

	public Team(String name) {
		this.teamName = name;
	}
	
	public Team(String name, int points,List<User> users,List<Achievement> achievements){
		this.teamName = name;
		this.points = points;
		this.teamAchievements = achievements;
		this.teamMembers = users;
	}

	public int getPoints() {
		int total = 0;
		System.out.println("----------------------------------");
		System.out.println("Summing points for team "+getName());
		for(Achievement e : teamAchievements){
			System.out.println("+"+e.getPoints());
			total += e.getPoints();
		}
		System.out.println("Team had "+total+" points.");
		return total;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void addPoints(int points) {
		this.points += points;
	}

	public String getName() {
		return teamName;
	}

	public List<User> getMembers() {
		return new ArrayList<User>(teamMembers);
	}

	public List<Achievement> getAchievements() {
		return new ArrayList<Achievement>(teamAchievements);
	}

	public boolean addMember(User member) {
		return teamMembers.add(member);
	}

	public boolean addAchievement(Achievement ach) {
		return teamAchievements.add(ach);
	}
	
	public void addAchievements(ArrayList<Achievement> ach) {
		teamAchievements.addAll(ach);
	}
	
	public int compareTo(Team other) {
		return Integer.compare(other.getPoints(), this.getPoints());
	}

}
