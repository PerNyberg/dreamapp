$(document).ready(function(){
	
					$(document).on("click", "#logoutBtn", function () {
						alert("Logged out, removed cookies.");
						Cookies.remove("userID");
						window.location = '/login';
					});
						
					$(document).on("change", ".flipClassHome", function () {
						var liid = this.id;
						if($(this).val() == "off"){
							$($(this).val("on").flipswitch("refresh"));
						}else if($(this).val() == "on"){
							$($(this).val("off").flipswitch("refresh"));
						}
						$.ajax({
							 type: 'PUT',
							 url: '/toggleAttendance/'+Cookies.get("userID")+'/'+this.id,
							 success: function(){
								$("[id='li"+liid+"']").hide();
							 }
						});
					});
					
					$(document).on("change", ".flipClassSchedule", function () {
						var liid = this.id;
						if($(this).val() == "off"){
							$($(this).val("on").flipswitch("refresh"));
						}else if($(this).val() == "on"){
							$($(this).val("off").flipswitch("refresh"));
						}
						$.ajax({
							 type: 'PUT',
							 url: '/toggleAttendance/'+Cookies.get("userID")+'/'+this.id,
							 success: function(){
							 }
						});
					});

					$(document).on("click", ".teamListButton", function () {
						$.ajax({
							type: 'PUT',
							url: '/joinTeam/'+Cookies.get("userID")+'/'+this.id,
							success: function(){
								window.location = "/team/"+Cookies.get("userID");
								//$.mobile.pageContainer.pagecontainer("change", "/team/"+Cookies.get("userID"), {transition: "fade"});
							}
						});
					});
					
					
					
					$(document).on("click", "#achievementBtn", function () {
						if(document.URL != "/achievements/"+Cookies.get("userID")){
							window.location = "/achievements/"+Cookies.get("userID");
							//$.mobile.pageContainer.pagecontainer("change", "/achievements/"+Cookies.get("userID"), {transition: "fade"});
						}
					});
					
					$(document).on("click", "#achievementTeamBtn", function () {
						if(document.URL != "/achievementsTeam/"+Cookies.get("userID")){
							alert("Button was pressed");
							window.location = "/achievementsTeam/"+Cookies.get("userID");
							//$.mobile.pageContainer.pagecontainer("change", "/achievements/"+Cookies.get("userID"), {transition: "fade"});
						}
					});
					
					$(document).on("click", "#leaderboardsTeamBtn", function () {
						if(document.URL != "/leaderboardsTeam"){
							window.location = "/leaderboardsTeam";
							//$.mobile.pageContainer.pagecontainer("change", "/achievements/"+Cookies.get("userID"), {transition: "fade"});
						}
					});
					
					
					$(document).on("click", "#homeBtn", function () {
						if(document.URL != "/home/"+Cookies.get("userID")){
							window.location = "/home/"+Cookies.get("userID");
							//$.mobile.pageContainer.pagecontainer("change", "/home/"+Cookies.get("userID"), {transition: "fade"});
						}
					});
					
					$(document).on("click", "#scheduleBtn", function () {
						if(document.URL != "/schedule/"+Cookies.get("userID")){
							window.location = "/schedule/"+Cookies.get("userID");
							//$.mobile.pageContainer.pagecontainer("change", "/home/"+Cookies.get("userID"), {transition: "fade"});
						}
					});
					
					$(document).on("click", "#teamBtn", function () {
						if(document.URL != "/team/"+Cookies.get("userID")){
							window.location = "/team/"+Cookies.get("userID");
							//$.mobile.pageContainer.pagecontainer("change", "/team/"+Cookies.get("userID"), {transition: "fade"});
						}
					});

});